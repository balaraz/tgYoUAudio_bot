# Run in container

You need to install `podman` before running. [Introduce to `podman`][podman-install]

## 1. Build image

```
podman build -t youmus .
```

## 2. Run container

```
podman run --rm youmus
```

You can add `-it` to enable interactive mode.

> If you want to use `docker` replace `podman` to `docker` in commands up.


[podman-install]: https://podman.io/docs/installation
