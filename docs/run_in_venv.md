# Install in virtual enviroment


## 1. Install requirements

You need to install `poetry` before installing dependencies. [Introduce to `poetry`][poetry-install]

To install all requirements install `poetry` and run:

```
poetry install
```

This command will create a virtual environment and install all the necessary dependencies into it.

## 2. Run

Now we are ready to launch the server developers

```bash
poetry run python ./app
```

> The webhook launch is not ready yet.


[poetry-install]: https://python-poetry.org/docs/
