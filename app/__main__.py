#!/usr/bin/env python

""" Main module to run bot. """

import asyncio
from aiogram import Bot, Dispatcher
from loguru import logger

from config import cfg, cache # pylint: disable=wrong-import-order
from handlers import router



@logger.catch
async def main() -> None:
    """ Main async function. Run despatcher. """

    token: str = cfg.get("bot.token")

    bot = Bot(token = token)
    dp = Dispatcher()

    dp.include_router(router)

    await asyncio.wait(
        [
            asyncio.create_task(dp.start_polling(bot)),
            asyncio.create_task(cache.clear(cfg.cache.clear_timeout))
        ],
        return_when=asyncio.FIRST_COMPLETED
    )



if __name__ == '__main__':
    asyncio.run(main())
