""" This module contains parsing arguments """

from argparse import ArgumentParser
from pathlib import Path



def to_dir_path(p: str) -> Path:
    """ Check path to exists if true return Path object """
    path = Path(p)
    if not path.is_dir():
        raise ValueError("path is not exists directory")
    return path


parser = ArgumentParser(
    description="An asyncthrone Telegram bot for download audio from YouTube.",
)

parser.add_argument(
    '-l', '--level',
    dest="logs.level",
    choices={"DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"},
    help="Set logging level",
)

parser.add_argument(
    "-p", "--path",
    dest="logs.path",
    metavar="dir_path",
    type=to_dir_path,
    help="Set logs path",
)

parser.add_argument(
    "-d", "--dir",
    dest="download.dir",
    metavar="dr_path",
    type=to_dir_path,
    help="Set download directory",
)

reuse_group = parser.add_mutually_exclusive_group()
reuse_group.add_argument('-r', '--reuse',
                         action='store_const',
                         const=True,
                         dest="download.reuse",
                         help='Reuse old data')
reuse_group.add_argument('-R', '--no-reuse',
                         action='store_const',
                         const=False,
                         dest="download.reuse",
                         help='Rewrite old data')

parser.add_argument(
    "-c", "--clear-cache",
    type=int,
    dest="cache.clear_timeout",
    metavar="secs",
    help="Set clear cache timeout",
)


args = parser.parse_args()
