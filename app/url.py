""" Module contains url check functions. """

from urllib.parse import urlparse, ParseResult
from urllib.request import urlopen



def is_valid_url(url: str) -> bool:
    """ Check string to contains url. """
    try:
        result = urlparse(url)
        return all([result.scheme, result.netloc])
    except ValueError:
        return False



class Url:
    """ Provides url checks methods """

    def __init__(self, url: str):
        if not isinstance(url, str):
            raise TypeError("url argument required type str")

        self._parsed_url_: ParseResult = urlparse(url)

    def __str__(self) -> str:
        return self._parsed_url_.geturl()

    def __repr__(self) -> str:
        return f"<Url '{self._parsed_url_.geturl()}'>"

    def is_valid(self) -> bool:
        """ Check url to exists """
        return all((self._parsed_url_.scheme, self._parsed_url_.netloc))

    def is_exists(self) -> bool:
        """ Check response status code to equality 200 """
        with urlopen(self._parsed_url_.geturl()) as response:
            return response.getcode() == 200

    def check_host(self, *hosts: str) -> bool:
        """ Check hostname """

        for host in hosts:
            if not isinstance(host, str):
                raise TypeError("All arguments required str type")

        if self._parsed_url_.hostname in hosts:
            return True
        return False
