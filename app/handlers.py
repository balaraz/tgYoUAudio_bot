""" Bot handlers and bind to dispatcher """

from pathlib import Path

from aiogram import Router, F
from aiogram.types import Message, ContentType, FSInputFile
from aiogram.filters import Command
from loguru import logger

from url import Url
from filters import URLFilter, YouTubeURLFilter
from download import download_audio_only
from convert import convert_to_audio



router = Router()


@router.message(Command("start"))
@router.message(Command("help"))
async def start(message: Message):
    """ Hander for /start command. Print welcome info. """
    await message.reply(
        "Вітаю! Я бот для завантаження музики з YouTube."
        "Для завантаження можете надіслати мені прото посилання і я надіжлю вам аудіо файл у відповідь."
    )


@router.message(F.content_type==ContentType.TEXT, YouTubeURLFilter())
async def download(message: Message):
    """ Hander to download command. """

    logger.debug("Start download handler")

    if not message.text:
        logger.debug("Message text is empty")
        return

    url = Url(message.text)

    logger.debug("Update status to check url")

    ans: Message = await message.reply("Перевірка посилання...")

    logger.debug("Checking url")

    if not url.is_valid():
        await ans.edit_text("Некоректне посилання.")
        return
    if not url.is_exists():
        await ans.edit_text("За цим покликання зміст недоступний")
        return

    logger.debug("Update status to downloading")

    await ans.edit_text("Завантаження відео...")

    logger.debug("Tring to dowload audio")

    try:
        audio_path: Path = await download_audio_only(url)
        logger.success("Audio downloaded")
    except Exception:
        logger.warning("Audio downloading failed")
        await ans.edit_text("Помилка завантаження")
        return

    logger.debug("Checking audio path")

    if not audio_path.exists():
        logger.debug("Audio path is not exists")
        await ans.edit_text("Помилка: файл незнайдено.")
        return
    if not audio_path.is_file():
        logger.debug("Audio path is not file")
        await ans.edit_text("Помилка: не файл.")
        return

    if audio_path.suffix != ".mp3":
        logger.debug("Update status to converting")

        await ans.edit_text("Конвертація у mp3...")

        try:
            audio_path: Path = await convert_to_audio(audio_path)
        except Exception:
            logger.warning("Converting to mp3 failed")
            await ans.edit_text("Помилка конвертації")
            return

        logger.debug("Checking audio path")

        if not audio_path.exists():
            logger.debug("Audio path is not exists")
            await ans.edit_text("Помилка: файл незнайдено.")
            return
        if not audio_path.is_file():
            logger.debug("Audio path is not file")
            await ans.edit_text("Помилка: не файл.")
            return

    logger.debug("Update status to sending")

    await ans.edit_text("Надсилання...")

    logger.debug("Sending file")

    audio_file = FSInputFile(str(audio_path))
    await message.reply_audio(audio_file)

    logger.debug("Delete status message")

    await ans.delete()


@router.message(F.content_type==ContentType.TEXT, URLFilter())
async def other_urls(message: Message):
    """ Handler to not youtube links. """
    await message.reply("Це посилання не на YouTube. Я вмію завантажувати тільки з нього.")


@router.message(F.content_type==ContentType.TEXT)
async def other_messages(message: Message):
    """ Handler to not links. """
    await message.reply("Це не посилання. Я розумію тільки посилання на YouTube.")
