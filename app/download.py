""" Module contains download functions. """

from pathlib import Path

from aiotube import Video
from loguru import logger

from url import Url
from config import cfg, cache



@logger.catch(reraise=True)
@cache.memorize
async def download_audio_only(url: Url) -> Path:
    """ Download only audio from YouTube. """

    if not isinstance(url, Url):
        raise TypeError("type of 'url' argument is not Url")

    output_dir: Path = cfg.get("download.dir", Path('/tmp'))

    assert output_dir.exists(), f"Output dir is not exists: {output_dir}"
    assert output_dir.is_dir(), f"Output path is not directory: {output_dir}"

    logger.debug("Connecting to the url")

    video = Video(str(url))

    logger.debug("Selecting a stream to download")

    streams = await video.streams()
    audio_stream = streams.get_audio_only()

    if not audio_stream:
        raise ValueError("Not found audio stream")

    file_name = f"{audio_stream.author} - {audio_stream.title}.{audio_stream.subtype}"
    output_path = output_dir / file_name

    if output_path.exists():
        if not output_path.is_file():
            raise ValueError("Output path exists but not file")
        if cfg.get("download.reuse", True):
            return output_path

    logger.debug("Downloading begins")

    await audio_stream.download_filepath(filename=str(output_path))

    logger.debug("Download completed successfully")

    return output_path
