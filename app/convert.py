""" Module contains converting functions. """

from pathlib import Path

from ffmpeg.asyncio import FFmpeg
from loguru import logger

from config import cache



@logger.catch(reraise=True)
@cache.memorize
async def convert_to_audio(input_path: Path) -> Path:
    """ Convert input file to audio file.  """

    if not isinstance(input_path, Path):
        raise TypeError("input_file argument is not instance of pathlib.Path")
    if not input_path.exists():
        raise ValueError("input path is not exists")
    if not input_path.is_file():
        raise ValueError("input path is not file")

    logger.debug("Generating output file path")

    output_path: Path = input_path.with_suffix(".mp3")

    logger.debug("Creating FFmpeg object")

    ffmpeg = (
        FFmpeg()
        .option("y")
        .input(str(input_path))
        .output(str(output_path))
    )

    logger.debug("Start executing ffmpeg")

    await ffmpeg.execute()

    logger.debug("Checks output path")

    assert output_path.exists(), f"Audio path is not exists: {output_path}"
    assert output_path.is_file(), f"Audio path is not file: {output_path}"

    return output_path
