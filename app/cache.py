""" Module contains caching functionality """

from time import time
from functools import wraps
from typing import Any, Callable, TypeVar

from asyncio import sleep
from loguru import logger
from super_hash import super_hash



class Cache:
    """ Cache object """

    _T = TypeVar("_T") # pylint: disable=C0103

    def __init__(self, *,
                 timeout: int,
                 on_delete: Callable[[_T], Any] = lambda x: None,
                 check_is_valid: Callable[[_T], bool] | None = None):
        self._data_: dict = {}

        self._timeout_ = timeout

        self._on_delete_ = on_delete
        self._check_is_valid_ = check_is_valid


    def memorize(self, func: Callable):
        """ Decorator for memorize functions """
        @wraps(func)
        async def wrapper(*args, **kwargs):
            key = super_hash((args, kwargs))
            if not self._data_.get(key):
                logger.debug(f"Caching data for {func.__name__} function with hash {key}")
                self._data_[key] = time(), (await func(*args, **kwargs))
            else:
                if self._check_is_valid_:
                    logger.debug(f"Check on valid {key} for function {func.__name__}")
                    if not self._check_is_valid_(self._data_[key][1]):
                        logger.debug(f"Cache is not valid for function {func.__name__} at hash {key}")
                        self._data_[key] = time(), (await func(*args, **kwargs))
                logger.debug(f"Use cached data for {func.__name__} function with key")
                self._data_[key] = time(), self._data_[key][1]
            return self._data_[key][1]
        return wrapper


    async def clear(self, timeout: int = 60):
        """ Asyncthonius task to clear cache """
        while True:
            await sleep(timeout)
            number_deleted = 0
            logger.info("Start clearing cache")
            for k in tuple(self._data_.keys()):
                (t, d) = self._data_[k]
                logger.debug(f"Check cache key {k}")
                if time()-t > self._timeout_:
                    logger.debug(f"Remove cache data {k}")
                    self._on_delete_(d)
                    del self._data_[k]
                    number_deleted += 1
            logger.info(f"Stop clearing cache. Cleared {number_deleted} items")
