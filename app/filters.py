""" Module contains message filters """

from aiogram.filters import Filter
from aiogram.types import Message
from loguru import logger

from url import Url



class URLFilter(Filter):
    """ Aiogram library messages filter. """

    def __init__(self, *hosts: str):
        self._hosts = hosts

    async def __call__(self, msg: Message) -> bool:
        if not msg.text:
            logger.debug("Filting empty message")
            return False

        try:
            url = Url(msg.text)
        except ValueError:
            logger.debug("Message is not url")
            return False

        if not url.is_valid():
            logger.debug("Message is not valid url")
            return False
        if not url.is_exists():
            logger.debug("Message is valid but not exists")
            return False

        if self._hosts:
            logger.debug("Checking url hostneme")
            return url.check_host(*self._hosts)

        return True



class YouTubeURLFilter(URLFilter):
    """ Aiogram library messages YouTube url filter. """
    # pylint: disable=too-few-public-methods

    def __init__(self):
        super().__init__(
            "www.youtube.com",
            "youtube.com",
            "youtu.be",
            "music.youtube.com",
        )
