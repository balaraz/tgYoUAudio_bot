""" Module load and check configuration """

import sys
from pathlib import Path

from dynaconf import Dynaconf, Validator, ValidationError
from loguru import logger
from loguru_logging_intercept import setup_loguru_logging_intercept

from args import args
from cache import Cache



cfg = Dynaconf(
    envvar_prefix="YOUMUS",
    settings_files=['config/settings.toml', 'config/.secrets.toml'],
    validators=[
        Validator("download.dir", cast=Path),
        Validator("download.reuse", cast=bool),

        Validator("cache.clear_timeout", cast=int),
        Validator("cache.expire_timeout", cast=int),

        Validator("logs.path", cast=Path),
        Validator("bot.token", must_exist=True, cast=str)
    ],
)


try:
    cfg.validators.validate_all()
except ValidationError as vex:
    for detail in vex.details:
        logger.critical(detail[1])
    sys.exit(1)


for k, v in args.__dict__.items():
    if not v is None:
        cfg.set(k, v)


cfg.setdefault("download.dir", Path('/tmp/youmus'))
cfg.setdefault("download.reuse", True)

cfg.setdefault("cache.clear_timeout", 5*60)
cfg.setdefault("cache.expire_timeout", 10*60)

cfg.setdefault("logs.level", "INFO")
cfg.setdefault("logs.path", "./logs")
cfg.setdefault("logs.name", "{time:YY-MM-DD}.log")
cfg.setdefault("logs.rotation", "1 day")
cfg.setdefault("logs.serialize", False)


for field in ("download.dir", "logs.path"):
    path = cfg.get(field)
    if not path.exists():
        path.mkdir()
    if not path.is_dir():
        logger.critical(f"'{field}' exist but is not directory")
        sys.exit(1)


# Configure logger
logger.remove(0)
logger.add(
    sys.stdout,
    level=cfg.get("logs.level"),
    colorize=True,
    enqueue=True,
)

logger.add(
    cfg.get("logs.path") / cfg.get("logs.name"),
    level=cfg.get("logs.level"),
    rotation=cfg.get("logs.rotation"),
    serialize=cfg.get("logs.serialize"),
    enqueue=True,
)

setup_loguru_logging_intercept(
    logger.level(cfg.get("logs.level", "INFO")).no
)


# Configure cache
cache = Cache(
    timeout = cfg.cache.expire_timeout,
    on_delete = lambda path: path.unlink() if isinstance(path, Path) and path.exists() else None,
    check_is_valid = lambda path: path.is_file() if isinstance(path, Path) else False
)
