# tgYoUAudio bot

An asynchronous Telegram bot for downloading audio from YouTube.

## 1. Configure

Create a `.secrets.toml' file with your tokens. You can use the template file located in the `docs/`` directory.

```bash
 $ cp docs/.secrets.toml config/
```

After that, you need to insert the bot token into the `bot.token` field.

```toml
[bot]
TOKEN = "<Your telegram bot token>"
```

You also need to copy the main configuration file from `docs/`` to `.`. And edit it and not the template in `docs/`.

```bash
 $ cp docs/settings.toml config/
```

## Install & Run

You can run it two ways.

+ [Run in virtual envriroment (poetry)](docs/run_in_venv.md)
+ [Run in cotainer (docker/podman)](docs/run_in_container.md)
