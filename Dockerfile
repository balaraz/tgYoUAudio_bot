FROM python:3.11-alpine

WORKDIR /app

RUN apk update && apk add poetry

COPY pyproject.toml poetry.lock .
RUN poetry config virtualenvs.create false
RUN poetry install --no-dev

COPY ./app/ .

ENTRYPOINT ["poetry", "run", "python", "."]
